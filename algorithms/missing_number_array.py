"""
Find the missing number in an array
"""


def find_missing_number(arr_size, num_arr):
    """
    Given the size of an array and an array number integers,
    find the missing number and print it to the console.
    :param size: The size of an array
    :param num_arr: An unsorted array that contains one missing number
    :return: The missing number
    """
    all_nums = list(range(1, arr_size + 1))
    print(sum(all_nums) - sum(num_arr))


if __name__ == '__main__':
    size = input()
    arr = list(map(int, input().rstrip().split()))

    find_missing_number(size, arr)
