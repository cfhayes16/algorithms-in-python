"""
Algorithms for the twosum problem.
Refer to https://leetcode.com/problems/two-sum
"""
from typing import List


def twosum(nums: List[int], target: int) -> List[int]:
    """
    Brute force implementation. This is an inefficient algorithm. It has a
    time complexity of O(N).
    """
    for i in range(len(nums)):
        for j in range(i + 1, len(nums)):
            if nums[j] == target - nums[i]:
                return [i, j]


def twosum_hashtable(nums: List[int], target: int) -> List[int]:
    """
    This function has improved performance over the twosum_brute_force
    function. It has a time complexity of O(1).
    """
    hashmap = {}
    for i in range(len(nums)):
        hashmap[nums[i]] = i
    for i in range(len(nums)):
        compliment = target - nums[i]
        if compliment in hashmap and hashmap[compliment] != i:
            return [i, hashmap[compliment]]
