"""
An example of the Euclid algorithm
"""


def gcd(num_a, num_b):
    """
    Calculates the greatest common denominator of two integers
    """
    while num_b != 0:
        temp = num_a
        num_a = num_b
        num_b = temp % num_b
    return num_a
