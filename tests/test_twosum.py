"""
Tests for twosum algorithm
"""
import pytest
from algorithms.twosum import twosum, twosum_hashtable

testdata = [
    ([3,3], 6, [0,1]),
    ([2,7,11,15], 9, [0,1]),
    ([3,2,4], 6, [1,2]),
    ([3,2,4,12,1,19,7,10], 13, [0,7])
]

@pytest.mark.parametrize("input, target, output", testdata)
def test_twosum_brute_force(input, target, output, capsys):
    results = twosum(input, target)
    print(results)
    captured = capsys.readouterr()
    assert captured.out == f'{output}\n'

@pytest.mark.parametrize("input, target, output", testdata)
def test_twosum_two_pass_hashtable(input, target, output, capsys):
    results = twosum_hashtable(input, target)
    print(results)
    captured = capsys.readouterr()
    assert captured.out == f'{output}\n'