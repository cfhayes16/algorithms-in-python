"""
Tests for finding the missing number in an array
"""
import pytest
from algorithms.missing_number_array import find_missing_number

testdata = [
    (8, [3, 7, 1, 2, 8, 4, 5], 6),
    (6, [1, 4, 2, 3, 6], 5),
]


@pytest.mark.parametrize("size, arr, expected", testdata)
def test_missing_number(size, arr, expected, capsys):
    """
    Parameterized test for missing number problem
    :param size: The size of an array
    :param arr: An array with one missing number
    :param expected: The missing number expected to be found
    :param capsys: Captures value printed from stdout
    :return: Nothing
    """
    find_missing_number(size, arr)
    captured = capsys.readouterr()
    assert captured.out == f'{expected}\n'
