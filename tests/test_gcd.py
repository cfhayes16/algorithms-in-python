"""
Tests for GCD (Greated Common Denominator)
"""
import pytest
from algorithms.gcd import gcd

testdata = [
    (20, 8, 4),
    (13, 27, 1),
    (256, 128, 128),
    (32, 16, 16),
    (0, 0, 0),
    (1, 1, 1)
]


@pytest.mark.parametrize("num_a, num_b,expected", testdata)
def test_scenario1(num_a, num_b, expected):
    """
    Given integers a and b, determine the GCD of the two numbers
    :param a: An integer
    :param b: An integer
    :param expected: The expected GCD
    :param capsys: Captures the stdout
    :return: Nothing
    """
    value = gcd(num_a, num_b)
    assert value == expected
