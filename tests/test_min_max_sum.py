"""
Tests for min_max_sum problem
"""
import pytest
from algorithms.min_max_sum import min_max_sum


testdata = [
    ([1, 2, 3, 4, 5], "10 14"),
    ([2, 4, 6, 8, 3], "15 21"),
    ([0, 0, 0, 0, 0], "0 0"),
]


@pytest.mark.parametrize("values, expected", testdata)
def test_scenario1(values, expected, capsys):
    """
    Given an unsorted array of integers, return the smallest and largest value
    :param values: An array of integers
    :param expected: The expected min/max values
    :param capsys: Captures the stdout
    :return: Nothing
    """
    min_max_sum(values)
    captured = capsys.readouterr()
    assert captured.out == f'{expected}\n'
    